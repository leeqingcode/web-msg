package com.czy.message.wscontroller;

import com.czy.message.annotation.WSController;
import com.czy.message.core.MessagePoster;
import com.czy.message.core.session.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.socket.WebSocketSession;

import java.util.Map;

/**
 * Created by lb on 2018/2/27.
 */
@WSController("/log")
public class LoginController {


    @Autowired
    SessionManager sessionManager;


    @RequestMapping("/login")
    public String login(Map map, WebSocketSession session){
        String uid = String.valueOf(map.get("uid"));
        if(StringUtils.isEmpty(uid)){
            return "parameter without uid";
        }
        //连接websocket服务器
        sessionManager.connect(uid, session);
        return "login success!";
    }

}
