package com.czy.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.logging.Logger;

/**
 *
 */
@SpringBootApplication
public class MessageApplication {

	static Logger logger = Logger.getLogger(MessageApplication.class.getName());

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(MessageApplication.class, args);
		logger.info("CZY-Message服务已启动！  port:" + context.getEnvironment().getProperty("server.port"));
		logger.info("-------------低调的分割线٩(๑❛ᴗ❛๑)۶------------");
	}
}
