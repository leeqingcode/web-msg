package com.czy.message.controller;

import com.czy.message.core.MessagePoster;
import com.czy.message.core.session.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

/**
 * 信息面板
 * Created by lb on 2017/12/13.
 */
@RestController
public class MainBoardController {

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private MessagePoster messagePoster;

    @RequestMapping("/listClients")
    public Object  index(){
        StringBuilder result = new StringBuilder();
        for(String key : sessionManager.getAllClientIds()){
            result.append("userId:" + key + "   cnt:" + sessionManager.getSession(key).size() + "<br>");
        }
        if(result.length() <= 0){
            return "no user.";
        }
        return result.toString();
    }

    @RequestMapping("/get")
    public void getMessage(){
        WebSocketSession webSocketSession = sessionManager.getSession("abc").get(0);
        try {
            messagePoster.sendMessage(webSocketSession,"上传完成");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("ddd");
    }

}
