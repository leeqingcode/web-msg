package com.czy.message.core;

import com.czy.message.core.session.DistributeSessionManager;
import com.czy.message.core.session.MultiSessionManager;
import com.czy.message.core.session.SessionManager;
import com.czy.message.core.session.SingleSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

@Configuration
public class MessageBeanConfig {


    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    MessageConfig messageConfig;

    @Bean
    public SessionManager sessionManager(){
        String mode = messageConfig.getSessionMode();
        if("single".equals(mode)){
            return new SingleSessionManager();
        }else if("multi".equals(mode)){
            return new MultiSessionManager();
        }else if("distribute".equals(mode)){
            return new DistributeSessionManager(applicationContext.getBean(StringRedisTemplate.class));
        }else{
            return new SingleSessionManager();
        }
    }

    //消息分发器
    @Bean
    public MessageDispatcher messageDispatcher(){
        return new MessageDispatcher();
    }

    //消息发送者
    @Bean
    public MessagePoster messageServer(SessionManager sessionManager){
        return new MessagePoster(sessionManager);
    }

    //消息内容
    @Bean
    public MessageContext messageContext(SessionManager sessionManager, MessageDispatcher messageDispatcher, MessagePoster messagePoster){
        return new MessageContext(sessionManager, messageDispatcher, messagePoster);
    }


    //消息处理器，接受消息
    @Bean
    public MessageHandler messageHandler(MessageContext messageContext){
        return new MessageHandler(messageContext);
    }


    //消息组件扫描
    @Bean
    public MessageComponentScan messageComponentScan(MessageDispatcher messageDispatcher){
        return new MessageComponentScan(messageDispatcher);
    }

}
